# Event sourcing PoC

## Initialization

SQL database is required as an event store.

Sample .env.local config for DB connection
```
DATABASE_URL="mysql://events:password@localhost:3307/events?unix_socket=/run/mysqld/mysqld.sock&serverVersion=10.6.4-MariaDB"
```

- `composer install`
- create DB user with rights to a db table
- configure DB connection in .env.local (see example above)
- `bin/console doctrine:database:create`
- `bin/console doctrine:schema:create`
- `symfony serve` (or other way of starting a local php server)

## API calls

Create user, Update user, Read user: see the MessageController.
Payload is identical to the respective Commands, see Command/CreateUser.php

The handlers are synchronous here, but in real world they would be asynchronous, and thus won't return the newly created user UUID.

## Example

```
POST http://localhost/user
Content-Type: application/json
{"name": "tester", "email": "test@test"}
```

(response contains the newly assigned UUID)

```
PATCH http://localhost/user
Content-Type: application/json
{"userId": "<UUID>", "email": "newmail@gmail.com"}
```

```
GET http://localhost/user/<UUID>
```

Look into the event store DB, as it should now contain 2 entries.

## 3-rd party software

This implementation is made from scatch using only Symfony framework and Doctrine ORM. There are some 3-rd party implementations available as well.
Most notable are **Prooph** and **Event Sauce**. They both claim they are intended mostly as a guidance, and Prooph even decided to deprecate
some of its parts in favor of ready-made symfony components.

There is also a cloud solution called **EventStore** worth considering.