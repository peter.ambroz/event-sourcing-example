<?php

declare(strict_types=1);

namespace App\Event;

use Symfony\Component\Uid\Uuid;

final readonly class UserUpdatedEvent
{
    public function __construct(
        public Uuid $userId,
        public ?string $email = null,
        public ?string $name = null,
        public ?string $passhash = null,
    ) {
    }

    public function __serialize(): array
    {
        return [
            'userId' => $this->userId->toRfc4122(),
            'email' => $this->email,
            'name' => $this->name,
            'passhash' => $this->passhash,
        ];
    }

    public function __unserialize(array $data): void
    {
        $this->userId = Uuid::fromString($data['userId']);
        $this->email = $data['email'];
        $this->name = $data['name'];
        $this->passhash = $data['passhash'];
    }
}
