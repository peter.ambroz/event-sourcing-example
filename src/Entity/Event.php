<?php

namespace App\Entity;

use App\Repository\EventRepository;
use DateTimeImmutable;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Uid\Uuid;

#[ORM\Entity(repositoryClass: EventRepository::class)]
#[ORM\Index(columns: ['model'], name: 'idx_model')]
#[ORM\Index(columns: ['uuid'], name: 'idx_uuid')]
class Event
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column]
    private ?DateTimeImmutable $ts;

    public function __construct(
        #[ORM\Column(length: 255)]
        private ?string $model,

        #[ORM\Column(length: 255)]
        private ?string $eventName,

        #[ORM\Column(type: 'uuid')]
        private ?Uuid $uuid,

        #[ORM\Column(type: 'object')]
        private ?object $data,
    ) {
        $this->ts = new DateTimeImmutable();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getModel(): ?string
    {
        return $this->model;
    }

    public function getEventName(): ?string
    {
        return $this->eventName;
    }

    public function getUuid(): ?Uuid
    {
        return $this->uuid;
    }

    public function getData(): object
    {
        return $this->data;
    }

    public function getTs(): ?DateTimeImmutable
    {
        return $this->ts;
    }
}
