<?php

declare(strict_types=1);

namespace App\Entity\ReadModel;

use App\Event\UserCreatedEvent;
use App\Event\UserUpdatedEvent;
use Exception;
use Symfony\Component\Uid\Uuid;

final class User
{
    private ?Uuid $userId = null;
    private string $email;
    private string $name;

    public function getUserId(): Uuid
    {
        return $this->userId;
    }

    public function getEmail(): string
    {
        return $this->email;
    }

    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @throws Exception
     */
    public function applyUserCreatedEvent(UserCreatedEvent $event): void
    {
        if ($this->userId !== null) {
            throw new Exception('User already created');
        }

        $this->userId = $event->userId;
        $this->email = $event->email;
        $this->name = $event->name;
    }

    public function applyUserUpdatedEvent(UserUpdatedEvent $event): void
    {
        if ($event->email !== null) {
            $this->email = $event->email;
        }

        if ($event->name !== null) {
            $this->name = $event->name;
        }
    }

    public function toArray(): array
    {
        return [
            'userId' => $this->userId->toRfc4122(),
            'email' => $this->email,
            'name' => $this->name,
        ];
    }
}
