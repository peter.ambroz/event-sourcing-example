<?php

declare(strict_types=1);

namespace App\Repository\ReadModel;

use App\Entity\ReadModel\User;
use App\Repository\EventRepository;
use Exception;
use Symfony\Component\Uid\Uuid;

final readonly class UserRepository
{
    public function __construct(private EventRepository $eventRepository)
    {
    }

    /**
     * @throws Exception
     */
    public function findById(Uuid $uuid): User
    {
        $user = new User();

        $events = $this->eventRepository->findBy(['model' => 'User', 'uuid' => $uuid], ['ts' => 'ASC']);

        foreach ($events as $event) {
            $method = sprintf('apply%s', $event->getEventName());

            if (method_exists($user, $method)) {
                $user->$method($event->getData());
            } else {
                throw new Exception(sprintf('Unable to apply event %s', $event->getEventName()));
            }
        }

        return $user;
    }
}
