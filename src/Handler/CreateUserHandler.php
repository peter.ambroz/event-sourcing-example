<?php

declare(strict_types=1);

namespace App\Handler;

use App\Command\CreateUser;
use App\Entity\Event;
use App\Event\UserCreatedEvent;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Messenger\Attribute\AsMessageHandler;
use Symfony\Component\Messenger\MessageBusInterface;
use Symfony\Component\Uid\Factory\UuidFactory;
use Symfony\Component\Uid\Uuid;

#[AsMessageHandler]
final readonly class CreateUserHandler
{
    public function __construct(
        private EntityManagerInterface $em,
        private UuidFactory $uuidFactory,
        private MessageBusInterface $bus,
    ) {
    }

    public function __invoke(CreateUser $command): Uuid
    {
        $userId = $this->uuidFactory->create();

        // todo validation

        $userCreatedEvent = new UserCreatedEvent($userId, $command->email, $command->name);

        // todo automatic model / event naming
        $event = new Event('User', 'UserCreatedEvent', $userId, $userCreatedEvent);
        $this->em->persist($event);
        $this->em->flush();

        $this->bus->dispatch($userCreatedEvent);

        return $userId;
    }
}
