<?php

declare(strict_types=1);

namespace App\Handler;

use App\Command\UpdateUser;
use App\Entity\Event;
use App\Event\UserUpdatedEvent;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Messenger\Attribute\AsMessageHandler;
use Symfony\Component\Messenger\MessageBusInterface;

#[AsMessageHandler]
final readonly class UpdateUserHandler
{
    public function __construct(private EntityManagerInterface $em, private MessageBusInterface $bus)
    {
    }

    public function __invoke(UpdateUser $command): void
    {
        // todo validation

        $userUpdatedEvent = new UserUpdatedEvent($command->userId, $command->email, $command->name, $command->passhash);
        $event = new Event('User', 'UserUpdatedEvent', $command->userId, $userUpdatedEvent);
        $this->em->persist($event);
        $this->em->flush();

        $this->bus->dispatch($userUpdatedEvent);
    }
}
