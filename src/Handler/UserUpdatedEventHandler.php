<?php

declare(strict_types=1);

namespace App\Handler;

use App\Event\UserUpdatedEvent;
use Symfony\Component\Messenger\Attribute\AsMessageHandler;

#[AsMessageHandler]
class UserUpdatedEventHandler
{
    public function __invoke(UserUpdatedEvent $event)
    {
    }
}
