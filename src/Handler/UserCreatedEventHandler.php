<?php

declare(strict_types=1);

namespace App\Handler;

use App\Event\UserCreatedEvent;
use Symfony\Component\Messenger\Attribute\AsMessageHandler;

#[AsMessageHandler]
class UserCreatedEventHandler
{
    public function __invoke(UserCreatedEvent $event)
    {
    }
}
