<?php

declare(strict_types=1);

namespace App\Command;

use Symfony\Component\Uid\Uuid;

final readonly class UpdateUser
{
    public function __construct(
        public Uuid $userId,
        public ?string $email = null,
        public ?string $name = null,
        public ?string $passhash = null,
    ) {
    }
}
