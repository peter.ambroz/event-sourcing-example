<?php

declare(strict_types=1);

namespace App\Controller;

use App\Command\CreateUser;
use App\Command\UpdateUser;
use App\Repository\ReadModel\UserRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Attribute\MapRequestPayload;
use Symfony\Component\Messenger\HandleTrait;
use Symfony\Component\Messenger\MessageBusInterface;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Uid\Uuid;

class MessageController extends AbstractController
{
    use HandleTrait;

    public function __construct(private MessageBusInterface $messageBus, private UserRepository $userRepository)
    {
    }

    #[Route('/user', name:'createUser', methods: ['POST'])]
    public function createUser(#[MapRequestPayload] CreateUser $createUser): Response
    {
        $uuid = $this->handle($createUser);
        assert($uuid instanceof Uuid);

        return $this->json(['userId' => $uuid->toRfc4122()], Response::HTTP_CREATED);
    }

    #[Route('/user/{uuid}', name:'readUser', methods: ['GET'])]
    public function readUser(Uuid $uuid): Response
    {
        $user = $this->userRepository->findById($uuid);

        return $this->json($user->toArray());
    }

    #[Route('/user', name: 'updateUser', methods: ['PATCH'])]
    public function updateUser(#[MapRequestPayload] UpdateUser $updateUser): Response
    {
        $this->messageBus->dispatch($updateUser);

        return $this->json([]);
    }
}
